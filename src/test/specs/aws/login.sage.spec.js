import '../../../env';
import Page from '../../../models/Page';
const Users = require('../../../fixtures/users.sage');

fixture `login test`
  .page `https://sageAPA.com/`;

test('Base Login & Logout Functionality', async t => {
  const page = new Page();

  await t // Login
    .maximizeWindow()
    .expect(page.selectors.container.login.visible).ok()
    .expect(page.login(Users.admin.username, Users.admin.password)).ok();

  await t // Logout
    .expect(page.selectors.navMenu.main.visible).ok()
    .expect(page.logout()).ok();

});
