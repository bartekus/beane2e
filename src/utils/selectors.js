module.exports = Object.freeze({
  body: Object.freeze({
    id: 'document.body.id',
    class: 'document.body.class',
  }),
  navmenu: Object.freeze({
    main: '#mainNavmenu',
  }),
  container: Object.freeze({
    app: '#appContainer',
    login: '#loginContainer',
  }),
  approvalChannels : Object.freeze({
    approvalChannelAccordion: '#approvalChannelAccordion',
    approvalChannelLegalEntity: '#s2id_legalEntity a.select2-choice',
    invoiceApprovalChannel: '#invoicesapprovalChannelsLink',
  }),
  beanboard : Object.freeze({
    beanboardTab: 'a.module-link.beanboard-link',
    userPerformanceHeader: 'h5=User Performance',
    userPerformanceWidget: '#userPerformanceWidget'
  }),
  general: Object.freeze({
    dataGrid: Object.freeze({
      firstTableRow: '.monetary-table-container table tbody tr:first-child',
      nextPage: 'a*=Next',
      numberOfPagesBox: '.dataTables_length select',
      pagerText: 'span.total-row-count',
      previousPage: 'a*=Previous',
      tableScroll: '.dataTables_scroll',
    }),
    upload: Object.freeze({
      uploadImages: '#uploadImages',
      uploadFile: '.fileinput-button input',
      confirmUploadFile: 'label.capture-btn.btn.btn-secondary input',
      saveUploadedImage: 'div.ui-dialog-buttonset button.save-images',
    }),
    button: Object.freeze({
      save: '#save',
    }),
    alertMessage: 'div.alert-danger',
    arrivalList: '#arrivalsList',
    arrivalListFirstRow: '#arrivalsList .backgrid tr:nth-child(1) td:nth-child(1) input',
    changePassword: '#changePassword',
    codeAbleItemFilter: '#myCodeableItemFilter',
    codingButtons: '#codingButtons',
    codingPanel: '#codingPanel',
    codingPanelContainer: '#codingPanelContainer',
    disabledStatus: 'input#status.disabled',
    editProfile: '#editProfile',
    erpLink: '#erpLink',
    errorVisible: 'div.error.visible',
    fullNameColumn: '.monetary-table-container tbody tr td:nth-child(8)',
    imageBox: 'img.fancybox-image',
    inProgressHeader: 'span=In-Progress',
    lineItemsAccordion: '#line-items-accordion',
    listLink: '#listsLink',
    managementTable: '#managementTableReportDD',
    managementTableAction: '#managementTableActionsDD',
    ownerText: 'span*=Verifier',
    legalEntitySelect: 'div#s2id_legalEntity a.select2-choice',
    lineItems: 'span*=LINE ITEMS',
    newPassword: '#password',
    repeatPassword: '#repeatPassword',
    searchLegalEntity: 'span*=Search for a Legal Entity',
    searchForVendor: 'span*=Search for a Vendor',
    subtotalText: 'tfoot tr:last-child td:last-child span.summary-numeric',
    userLink: '#usersLink',
    vendorInput: 'div#select2-drop input.select2-input',
    viewDetails: 'li a.action.view-details',
    viewImageLink: 'li a.action.view-image',
    viewProfile: '#viewProfile',
    yourPassword: '#yourPassword',
    zoomInIcon: 'i.view-pages.icon-zoom-in'
  }),
  invoices: Object.freeze({
    createInvoiceButton: '#createInvoices',
    invoiceDateDisplay: '#invoiceDateDisplay',
    invoiceHeader: 'span=Invoices',
    invoiceImagePane: '#invoiceImagePane',
    imageSelection: 'input[tabindex="-1"]',
    moduleLink: 'a.module-link.invoices-link',
    saveInvoice: '#saveInvoice',
    headerDetailsContent: '#headerDetailsContent',
    autocompleteLegalEntity: '#autocompleteLegalEntity',
    headerDetailsContainer: '#headerDetailsContainer',
    legalEntityInput: '#legalEntity',
    legalEntityDropdownStart: '#autocompleteLegalEntity a span.select2-arrow',
    legalEntityDropdownResults: '#select2-drop ul.select2-results',
    select2Dropdown: '#select2-drop',
  }),
  login : Object.freeze({
    firstName: '#firstname',
    lastName: '#lastname',
    password: 'input[type=password]',
    proceedBtn: 'button.proceedBtn',
    signIn: '#submit-login',
    signOut: '#signOut',
    username: '#username',
  }),
  monetary: Object.freeze({
    dueDateDisplay: '#dueDateDisplay',
    description: '#description',
    subtotal: '#subtotal',
    total: '#total',
  }),
  purchaseOrder: Object.freeze({
    header: 'span=Purchase Orders',
    fullNameColumn: '.monetary-table-container tbody tr td:nth-child(9)',
    moduleLink: 'a.module-link.pos-link',
  }),
  settings: Object.freeze({
    batchCreateUser : Object.freeze({
      userBatch: '#userBatch',
    }),
    settingMenuButton: '#settings',
    settingMenu: '.dropdown-menu .dropdown-menu-right',
    viewSettings: '#viewSettings',
    logout: '#signOut i',
  }),
  workflowNavigation : Object.freeze({
    createInvoiceLink: 'a.section-link.create-link',
  }),
  misc: Object.freeze({
    wootricModal: '#wootric-modal',
    wootricModalCloseButton1: '#wootric-close',
    wootricModalCloseButton2: '#wootric-dismiss',
  })
});
