import { Selector, t } from "testcafe";

const Target = require('../utils/selectors');

export default class Page {
  constructor() {
    this.selectors = {
      body: {
        id: Selector(Target.body.id),
        class:  Selector(Target.body.class),
      },
      navMenu: {
        main: Selector(Target.navmenu.main),
      },
      navLink: {
        createInvoice: Selector('a[href$="#invoices/arrivals"]'),
      },
      container: {
        app: Selector(Target.container.app),
        login: Selector(Target.container.login),
        settingMenu: Selector(Target.settings.settingMenu),
      },
      input: {
        username: Selector(Target.login.username),
        password: Selector(Target.login.password),
      },
      button: {
        signIn: Selector(Target.login.signIn),
        createInvoice: Selector(Target.invoices.createInvoiceButton),
        save: Selector(Target.general.button.save),
        settings: Selector(Target.settings.settingMenuButton),
        signOut: Selector(Target.settings.logout),
      },
      general: {
        arrivalList: Selector(Target.general.arrivalList),
        headerDetailsContent: Selector(Target.invoices.headerDetailsContainer),
      },
      dropdown: {
        invoiceLegalEntityPick: Selector(Target.invoices.legalEntityDropdownStart),
        invoiceLegalEntityResults: Selector(Target.invoices.legalEntityDropdownResults),
      },
      checkbox: {
        selectImage: Selector(Target.invoices.imageSelection),
      },
      wootric: {
        modal: Selector(Target.misc.wootricModal),
        closeButton: Selector(Target.misc.wootricModalCloseButton1),
      }
    };
  }

  async login(user, password) {
    await t
      .typeText(this.selectors.input.username, user)
      .typeText(this.selectors.input.password, password)
      .click(this.selectors.button.signIn);
  }

  async logout() {
    await t
      .click(this.selectors.button.settings)
      .expect(this.selectors.button.signOut.visible).ok()
      .click(this.selectors.button.signOut);
  }
}
