import dotenv from 'dotenv';

dotenv.config({ silent: true });

console.log('.env has been loaded into process.env');
